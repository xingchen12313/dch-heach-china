import ResearchProjectApi from './research-project'
import CreateResearchProjectApi from './create-research-project'
import TemplateApi from './template-api'
import TemplateResultApi from './template-result-api'
import AuditApplyApi from './audit-apply'
import ResearchKindApi from './research-kind'
import TemplatePageGroupApi from './template-page-group'
import DataAnalysisApi from './data-analysis.js'
import TemplatePageApi from './Template-page'
export {
  ResearchProjectApi,
  CreateResearchProjectApi,
  TemplateApi,
  TemplateResultApi,
  AuditApplyApi,
  ResearchKindApi,
  TemplatePageGroupApi,
  DataAnalysisApi,
  TemplatePageApi
}
