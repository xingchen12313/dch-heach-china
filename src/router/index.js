import Vue from "vue";
import Router from "vue-router";
import routes from "./routers";
import store from "@/store";
import iView from "iview";
// eslint-disable-next-line no-unused-vars
import { canTurnTo, getDchToken, setDchToken } from "@/libs/util";
import config from "@/config";

const { homeName } = config;

Vue.use(Router);
const router = new Router({
    routes: routes,
    mode: "history"
});
const LOGIN_PAGE_NAME = "login";
// 路由跳转验证用户权限
// eslint-disable-next-line camelcase
// 豁免验证白名单 内为路由name值
const whiteList = ["report_url", 'login', 'register'];
const go_login = (next,type) => {
    // next()
    // window.location.href = config.login_url;
    console.log(process.env.VUE_APP_LOGIN_URL)
    if(process.env.VUE_APP_LOGIN_URL  === '/login.html'){
        if(type === 1){
            next('/login.html')
        }else{
            next()
        }
    }else{
        window.location.href = process.env.VUE_APP_LOGIN_URL;
    }
};

router.beforeEach((to, from, next) => {
    const token = getDchToken();
    if (whiteList.includes(to.name)) {
        next();
    } else if (
        to.meta.needLogin !== undefined &&
        to.meta.needLogin === false &&
        to.name !== LOGIN_PAGE_NAME
    ) {
        go_login(next,1);
    } else if (!token && to.name !== LOGIN_PAGE_NAME) {
        go_login(next,1);
    } else if (!token && to.name === LOGIN_PAGE_NAME) {
        // 未登陆且要跳转的页面是登录页
        go_login(next,1);
    } else if (token && to.name === LOGIN_PAGE_NAME) {
        // 已登录且要跳转的页面是登录页
        next({
            name: homeName // 跳转到homeName页
        });
    } else {
        // 已登录且要跳转的不是登录页
        if (store.state.user.hasGetInfo) {
            // 登录时获取用户信息成功
            // turnTo(to, store.state.user.access, next);
            next();
        } else {
            // 登录后未获取到用户信息，此时重新获取用户信息
            store
                .dispatch("getUserInfo")
                .then(res => {
                    // 成功获取用户信息以后,根据用户权限去判定是否可以跳转
                    // turnTo(to, user.access, next)
                    if (res.status === 200 && res.data.flag === true) {
                        next();
                    } else {
                        setDchToken("");
                        go_login(next);
                    }
                })
                .catch(() => {
                    // 未成功获取到用户信息，将Token设置为空，并跳转到登录页登录
                    setDchToken("");
                    go_login(next,1);
                });
        }
    }
});

router.afterEach(() => {
    iView.LoadingBar.finish();
    window.scrollTo(0, 0);
});

export default router;
