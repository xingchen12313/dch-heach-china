import config from "@/config";
import chinaRouters from './chinahealth/china-health'
import sysRouters from './sys/sys-routers'
import ResearchProjectRouters from './research-project'

let routers = []

if (config.sysName === 'ChinaHealth') {
  chinaRouters.forEach(item => {
    routers.push(item)
  })
}

if (config.sysName === 'ResearchPlatform') {
  ResearchProjectRouters.forEach(item => {
    routers.push(item)
  })
  sysRouters.forEach(item => {
    routers.push(item)
  })
}

export default routers
